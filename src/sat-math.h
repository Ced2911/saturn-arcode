// inspired by gsdk

#pragma once
//#include <iapetus.h>
typedef signed int fix32;


#define min(X, Y) (((X) < (Y)) ? (X) : (Y))
#define max(X, Y) (((X) > (Y)) ? (X) : (Y))
#define abs(X) (((X) < 0) ? -(X) : (X))

//
#ifndef PI
#define PI 3.14159265358979323846
#endif

//
#define FIX32_INT_BITS 22
#define FIX32_FRAC_BITS (32 - FIX32_INT_BITS)

#define FIX32_INT_MASK (((1 << FIX32_INT_BITS) - 1) << FIX32_FRAC_BITS)
#define FIX32_FRAC_MASK ((1 << FIX32_FRAC_BITS) - 1)


#define SIN_TABLE_LEN ((1 << FIX32_FRAC_BITS))
extern const fix32 sintab32[SIN_TABLE_LEN];

#if 1
#define sinFix32(value) sintab32[(value)&(SIN_TABLE_LEN-1)]
#define cosFix32(value) sintab32[((value) + (SIN_TABLE_LEN/4)) & (SIN_TABLE_LEN-1)]
#else
#define sinFix32(value) sintab32[(value)&2047]
#define cosFix32(value) sintab32[((value) + 512) & 2047]

#endif

//
#define FIX32(value) ((fix32)((value) * (1 << FIX32_FRAC_BITS)))

#define intToFix32(value) ((value) << FIX32_FRAC_BITS)
#define fix32ToInt(value) ((value) >> FIX32_FRAC_BITS)
#define fix32Round(value) \
    ((fix32Frac(value) > FIX32(0.5)) ? fix32Int(value + FIX32(1)) + 1 : fix32Int(value))
#define fix32ToRoundedInt(value) \
    ((fix32Frac(value) > FIX32(0.5)) ? fix32ToInt(value) + 1 : fix32ToInt(value))
#define fix32Frac(value) ((value)&FIX32_FRAC_MASK)
#define fix32Int(value) ((value)&FIX32_INT_MASK)
#define fix32Add(val1, val2) ((val1) + (val2))
#define fix32Sub(val1, val2) ((val1) - (val2))
#define fix32Neg(value) (0 - (value))
#define fix32Mul(val1, val2) (((val1) * (val2)) >> FIX32_FRAC_BITS)
#define fix32Div(val1, val2) (((val1) << FIX32_FRAC_BITS) / (val2))

typedef struct
{
    fix32 x;
    fix32 y;
    fix32 z;
} Vector3D;

typedef struct
{
    fix32 x;
    fix32 y;
    fix32 z;
    fix32 w;
} Vector4D;

typedef struct
{
    Vector4D a;
    Vector4D b;
    Vector4D c;
    //Vector4D d;
} Matrix3D;


void setResolution(int x, int y);
void setDrawPosition(fix32 x, fix32 y, fix32 z);

void matrixIdentity(Matrix3D *mat);
void matrixTranslate(Matrix3D *mat, fix32 x, fix32 y, fix32 z);
void matrixScaleXYZ(Matrix3D *mat, fix32 x, fix32 y, fix32 z);
void matrixScale(Matrix3D *mat, fix32 scale);


void matrixMultiply(Matrix3D *dst, Matrix3D *a, Matrix3D *b);
void matrixRotateX(Matrix3D *mat, fix32 r);
void matrixRotateY(Matrix3D *mat, fix32 a);
void matrixRotateZ(Matrix3D *mat, fix32 r);
void matrixRotate(Matrix3D *mat, fix32 x, fix32 y, fix32 z);
void matrixInv(Matrix3D *mat, Matrix3D *dest);

void matrixVectorMultiply(Matrix3D *mat, Vector3D *vec, Vector3D * dest);
void matrixVectorRotate(Matrix3D *mat, Vector3D *vec, Vector3D * dest);
void matrixTransform(Matrix3D *map, const Vector3D *src, Vector3D *dest, uint32_t numv);
void vectors3DProjects(const Vector3D *src, fix32 camDist, Vector3D *dest, uint16_t numv);


/**
 * same as sprite_struct but with more detail
 **/
typedef struct
{
   u32 link_addr;
   u32 attr;
   u32 addr;
   u32 gouraud_addr;
   u16 bank;
   u16 width, height;
   s16 x0;
   s16 y0;
   s16 x1;
   s16 y1;
   s16 x2;
   s16 y2;
   s16 x3;
   s16 y3;
   // added from sprite_struct
   s16 z_order;
} polygon_struct;

