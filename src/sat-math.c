#include <iapetus.h>
#include <stdint.h>
#include "sat-math.h"

static Vector3D drawPosition;

static int viewWidth;
static int viewHeight;

void setResolution(int x, int y)
{
    viewWidth = x;
    viewHeight = y;
}

void setDrawPosition(fix32 x, fix32 y, fix32 z)
{
    drawPosition.x = x;
    drawPosition.y = y;
    drawPosition.z = z;
}

void matrixIdentity(Matrix3D *mat)
{
    mat->a.x = FIX32(1);
    mat->a.y = 0;
    mat->a.z = 0;

    mat->b.x = 0;
    mat->b.y = FIX32(1);
    mat->b.z = 0;

    mat->c.x = 0;
    mat->c.y = 0;
    mat->c.z = FIX32(1);
}

void matrixTranslate(Matrix3D *mat, fix32 x, fix32 y, fix32 z)
{
    mat->a.x = FIX32(1);
    mat->a.y = 0;
    mat->a.z = x;

    mat->b.x = 0;
    mat->b.y = FIX32(1);
    mat->b.z = y;

    mat->c.x = 0;
    mat->c.y = 0;
    mat->c.z = FIX32(1);
}

void matrixMultiply(Matrix3D *dst, Matrix3D *a, Matrix3D *b)
{
    //dst->a.x = fix32Mul(dst->a.x)
}

// https://i.stack.imgur.com/Q159v.png
void matrixRotate(Matrix3D *mat, fix32 x, fix32 y, fix32 z)
{
    fix32 cx = cosFix32(x);
    fix32 sx = sinFix32(x);
    fix32 cy = cosFix32(y);
    fix32 sy = sinFix32(y);
    fix32 cz = cosFix32(z);
    fix32 sz = sinFix32(z);
    fix32 sxsy = fix32Mul(sx, sy);
    fix32 cxsy = fix32Mul(cx, sy);

#if 0
    mat->a.x = fix32Mul(cy, cz);
    mat->b.x = -fix32Mul(cy, cz);
    mat->c.x = sy;

    mat->a.y = ((sxsy * cz) + (cx * sz)) >> FIX32_FRAC_BITS;
    mat->b.y = ((cx * cz) - (sxsy * sz)) >> FIX32_FRAC_BITS;
    mat->c.y = -fix32Mul(sx, cy);

    mat->a.z = ((sx * sz) - (cxsy * cz)) >> FIX32_FRAC_BITS;
    mat->b.z = ((cxsy * sz) + (sx * cz)) >> FIX32_FRAC_BITS;
    mat->c.z = fix32Mul(cx, cy);
#else
    mat->a.x = fix32Mul(cy, cz);
    mat->b.x = fix32Mul(cy, sz);
    mat->c.x = -sy;

    mat->a.y = ((sxsy * cz) - (cx * sz)) >> FIX32_FRAC_BITS;
    mat->b.y = ((cx * cz) + (sxsy * sz)) >> FIX32_FRAC_BITS;
    mat->c.y = fix32Mul(sx, cy);

    mat->a.z = ((sx * sz) + (cxsy * cz)) >> FIX32_FRAC_BITS;
    mat->b.z = ((cxsy * sz) - (sx * cz)) >> FIX32_FRAC_BITS;
    mat->c.z = fix32Mul(cx, cy);
#endif
}

void matrixInv(Matrix3D *mat, Matrix3D *dest)
{
    dest->a.x = mat->a.x;
    dest->a.y = mat->b.x;
    dest->a.z = mat->c.x;

    dest->b.x = mat->a.y;
    dest->b.y = mat->b.y;
    dest->b.z = mat->c.y;

    dest->c.x = mat->a.z;
    dest->c.y = mat->b.z;
    dest->c.z = mat->c.z;
}

void matrixRotateY(Matrix3D *mat, fix32 a)
{
    fix32 c = cosFix32(a);
    fix32 s = sinFix32(a);

    mat->a.x = c;
    mat->a.y = 0;
    mat->a.z = s;

    mat->b.x = 0;
    mat->b.y = FIX32(1);
    ;
    mat->b.z = 0;

    mat->c.x = -s;
    mat->c.y = 0;
    mat->c.z = c;
}

void matrixRotateX(Matrix3D *mat, fix32 a)
{
    fix32 c = cosFix32(a);
    fix32 s = sinFix32(a);

    mat->a.x = FIX32(1);
    mat->a.y = 0;
    mat->a.z = 0;

    mat->b.x = 0;
    mat->b.y = c;
    mat->b.z = -s;

    mat->c.x = 0;
    mat->c.y = s;
    mat->c.z = c;
}

void matrixRotateZ(Matrix3D *mat, fix32 a)
{
    fix32 c = cosFix32(a);
    fix32 s = sinFix32(a);

    mat->a.x = c;
    mat->a.y = -s;
    mat->a.z = 0;

    mat->b.x = s;
    mat->b.y = c;
    mat->b.z = 0;

    mat->c.x = 0;
    mat->c.y = 0;
    mat->c.z = FIX32(1);
}

void matrixScale(Matrix3D *mat, fix32 scale)
{
    mat->a.x = scale;
    mat->a.y = 0;
    mat->a.z = 0;

    mat->b.x = 0;
    mat->b.y = scale;
    mat->b.z = 0;

    mat->c.x = 0;
    mat->c.y = 0;
    mat->c.z = scale;
}

void matrixScaleXYZ(Matrix3D *mat, fix32 x, fix32 y, fix32 z)
{
    mat->a.x = x;
    mat->a.y = 0;
    mat->a.z = 0;

    mat->b.x = 0;
    mat->b.y = y;
    mat->b.z = 0;

    mat->c.x = 0;
    mat->c.y = 0;
    mat->c.z = z;
}

void matrixVectorRotate(Matrix3D *mat, Vector3D *vec, Vector3D *dest)
{
    dest->x = fix32Mul(vec->x, mat->a.x) + fix32Mul(vec->y, mat->a.y) + fix32Mul(vec->z, mat->a.z);
    dest->y = fix32Mul(vec->x, mat->b.x) + fix32Mul(vec->y, mat->b.y) + fix32Mul(vec->z, mat->b.z);
    dest->z = fix32Mul(vec->x, mat->c.x) + fix32Mul(vec->y, mat->c.y) + fix32Mul(vec->z, mat->c.z);
}

void matrixVectorMultiply(Matrix3D *mat, Vector3D *vec, Vector3D *dest)
{
    // pas ok !!
    dest->x = (vec->x * mat->a.x + vec->y * mat->b.x + vec->z * mat->c.x) >> FIX32_FRAC_BITS;
    dest->y = (vec->x * mat->a.y + vec->y * mat->b.y + vec->z * mat->c.y) >> FIX32_FRAC_BITS;
    dest->z = (vec->x * mat->a.z + vec->y * mat->b.z + vec->z * mat->c.z) >> FIX32_FRAC_BITS;
}

void matrixTransform(Matrix3D *mat, const Vector3D *src, Vector3D *dest, uint32_t numv)
{
    fix32 *s;
    fix32 *d;
    uint32_t i;

    const fix32 tx = drawPosition.x;
    const fix32 ty = drawPosition.y;
    const fix32 tz = drawPosition.z;

    s = (fix32 *)src;
    d = (fix32 *)dest;
    i = numv;

    while (i--)
    {
        const fix32 sx = *s++;
        const fix32 sy = *s++;
        const fix32 sz = *s++;

        *d++ = fix32Mul(sx, mat->a.x) + fix32Mul(sy, mat->a.y) + fix32Mul(sz, mat->a.z) + tx;
        *d++ = fix32Mul(sx, mat->b.x) + fix32Mul(sy, mat->b.y) + fix32Mul(sz, mat->b.z) + ty;
        *d++ = fix32Mul(sx, mat->c.x) + fix32Mul(sy, mat->c.y) + fix32Mul(sz, mat->c.z) + tz;
    }
}

void vectors3DProjects(const Vector3D *src, fix32 camDist, Vector3D *dest, uint16_t numv)
{
    const Vector3D *s;
    Vector3D *d;
    fix32 zi;
    uint32_t wi, hi;
    uint32_t i;

    wi = (viewWidth) >> 1;
    hi = (viewHeight) >> 1;

    s = src;
    d = dest;
    i = numv;

    while (i--)
    {
        if ((zi = (s->z + camDist)) >= 0)
        {
        // Debug.. float
#if 0
            float fZi = (float)(zi)/(float)(FIX32(1));
            float fCamDist = 15;
            float sx = (float)(s->x >> 1) / (float)(FIX32(1));
            float sy = (float)(s->y) / (float)(FIX32(1));
            float sz = (float)(s->z) / (float)(FIX32(1));
            float fscale = (fCamDist*64.f)  /(fCamDist+sz);
            float dx = wi + sx * fscale;
            float dy = hi - sy * fscale;
#endif

            //zi = fix32Div(camDist << ((SIN_TABLE_LEN/256) + (SIN_TABLE_LEN/512)), camDist + zi);
            zi = fix32Div(camDist << (4+2), camDist + zi);
            //d->x = wi + fix32ToInt(fix32Mul(s->x, zi));
            //d->y = hi - fix32ToInt(fix32Mul(s->y, zi));
            d->x = wi + fix32ToInt(fix32Round(fix32Mul(s->x, zi)));
            d->y = hi - fix32ToInt(fix32Round(fix32Mul(s->y, zi)));
            // track z for ordering
            d->z = zi;
        }
        else
        {
            d->x = 0;
            d->y = 0;
            d->z = 0;
        }

        s++;
        d++;
    }
}

