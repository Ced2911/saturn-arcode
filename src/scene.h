#pragma once
#include "model.h"

void set_background_color(uint8_t r, uint8_t g, uint8_t b);
void set_clipping();
void modelDraw(model_t * model, Matrix3D * transformation, uint32_t gouraud_table_address);
void applyColorForPolygon(uint16_t *dst, Vector3D *norm, Vector3D *lightPosition, uint16_t *color);
void applyLightingForPolygon(uint16_t *dst, Vector3D *norm, Vector3D *lightPosition, uint16_t *color);

void sceneInit();
void sceneDraw();



