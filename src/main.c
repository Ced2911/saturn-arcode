#include <stdio.h>
#include <string.h>
#include <iapetus.h>
#include "config.h"
#include "boot_cd.h"
#include "sat-math.h"
#include "sprite-math.h"
#include "saturn.mdl.h"
#include "scene.h"


font_struct main_font;

void lib_init()
{
    screen_settings_struct settings;

    init_iapetus(RES_352x256);
    setResolution(352, 256);

    // Setup a screen for us draw on
    settings.is_bitmap = TRUE;
    settings.bitmap_size = BG_BITMAP512x256;
    settings.transparent_bit = 0;
    settings.color = BG_256COLOR;
    settings.special_priority = 0;
    settings.special_color_calc = 0;
    settings.extra_palette_num = 0;
    settings.map_offset = 0;
    settings.rotation_mode = 0;
    settings.parameter_addr = 0x25E60000;
    vdp_rbg0_init(&settings);

    // Use the default palette
    vdp_set_default_palette();

    // Setup the default 8x8 1BPP font
    main_font.data = font_8x8;
    main_font.width = 8;
    main_font.height = 8;
    main_font.bpp = 1;
    main_font.out = (u8 *)0x25E00000;
    vdp_set_font(SCREEN_RBG0, &main_font, 1);

    // HD mode...
    //vdp1_set_draw_mode(V1DM_AUTO, V1SM_HIGHRES, V1IM_NONINT);
    //vdp1_set_draw_mode(V1DM_AUTO, V1SM_HIGHRES, V1IM_NONINT);

    // Display everything
    vdp_disp_on();

    if (ud_detect() == IAPETUS_ERR_OK)
        cl_set_service_func(ud_check);
}



int main()
{
    u16 disc_type;
    int disc_error;

    lib_init();

    // Set background color
    set_background_color(0xFF, 0, 0);

    // draw...
    vdp_start_draw_list();
    set_clipping();
    vdp_end_draw_list();

    vdp_printf(&main_font, 8, 8, 0xF, "DD");

    sceneInit();

    while (1)
    {
        /*
        // Wait for cd...
        // If cd door closed boot cd
        if (is_cd_present())
        {
            if (is_audio_cd())
            {
                bios_run_cd_player();
            }

            if (!is_cd_auth(&disc_type))
            {
                jhl_cd_hack();

                if (!is_cd_auth(&disc_type))
                {
                    // Run CD Player
                    bios_run_cd_player();
                }

                disc_error = boot_cd();
            }
        }
        */

        // Check for input
        // do something related to input

        // draw

        vdp_start_draw_list();
        set_clipping();        
        sceneDraw();
        vdp_end_draw_list();

        vdp_vsync();
        vdp_vsync();
    }

    return 0;
}