#include <stdio.h>
#include <string.h>
#include <iapetus.h>
#include "config.h"
#include "sat-math.h"
#include "sprite-math.h"
#include "model.h"
#include "scene.h"
#include "sphere.h"
#include "cylinder.h"
#include "planet.h"

#define GOURAND_ADDR (0x40000)
#define GOURAND_PLANET_ADDR (0x41000)
#define GOURAND_SPHERE_ADDR (GOURAND_PLANET_ADDR + (planet_size * 4 * 8))
#define GOURAND_CYLINDER_ADDR (GOURAND_SPHERE_ADDR + (uvsphere_size * 4 * 8))

#define MAX_POINTS 4000

// scene-utls.c
uint8_t colorForPoint(Vector3D *lightPosition, Vector3D *norm);
int isPolygonCulled(sprite_struct *quad);

static Vector3D normal_3D[MAX_POINTS];
static Vector3D pts_3D[MAX_POINTS];
static Vector3D pts_2D[MAX_POINTS];
static Vector3D planet_pts_2D[MAX_POINTS];
static polygon_struct planet_pts_quad[MAX_POINTS];
static fix32 r = 0;
static Matrix3D transformation;

static int16_t planet_indices[MAX_POINTS];

void vectors3DProjectsToPoly(const Vector3D *src, fix32 camDist, polygon_struct *dest, uint16_t numv)
{
    const Vector3D *s;
    polygon_struct *quad;
    fix32 zi;
    uint32_t wi, hi;
    uint32_t i;

    wi = (SCREEN_WIDTH) >> 2;
    hi = (SCREEN_HEIGHT) >> 2;

    s = src;
    i = numv;

    while (i)
    {

        zi = fix32Div(camDist << (4 + 2), camDist + (s->z + camDist));
        dest->x0 = wi + fix32ToInt(fix32Round(fix32Mul(s->x, zi)));
        dest->y0 = hi - fix32ToInt(fix32Round(fix32Mul(s->y, zi)));
        dest->z_order = zi;
        s++;

        zi = fix32Div(camDist << (4 + 2), camDist + (s->z + camDist));
        dest->x1 = wi + fix32ToInt(fix32Round(fix32Mul(s->x, zi)));
        dest->y1 = hi - fix32ToInt(fix32Round(fix32Mul(s->y, zi)));
        dest->z_order += zi;
        s++;

        zi = fix32Div(camDist << (4 + 2), camDist + (s->z + camDist));
        dest->x2 = wi + fix32ToInt(fix32Round(fix32Mul(s->x, zi)));
        dest->y2 = hi - fix32ToInt(fix32Round(fix32Mul(s->y, zi)));
        dest->z_order += zi;
        s++;

        zi = fix32Div(camDist << (4 + 2), camDist + (s->z + camDist));
        dest->x3 = wi + fix32ToInt(fix32Round(fix32Mul(s->x, zi)));
        dest->y3 = hi - fix32ToInt(fix32Round(fix32Mul(s->y, zi)));
        dest->z_order += zi;
        s++;

        dest->z_order = dest->z_order / 4;

        dest++;
        i -= 4;
    }
}


static int compareVectorByZIndices(const void *a, const void *b)
{
    uint16_t ia = *(uint16_t *)(a);
    uint16_t ib = *(uint16_t *)(b);
    const polygon_struct *va = &planet_pts_quad[ia];
    const polygon_struct *vb = &planet_pts_quad[ib];
    return vb->z_order - va->z_order;
}

static void cylinderInit() {

    int i = 0;
    for (i = 0; i < cylinder_size; i++)
    {
        planet_indices[i + cylinder_size] = i + cylinder_size;
    }
    // sort by z
    qsort(planet_indices, planet_size, sizeof(uint16_t), compareVectorByZIndices);
}

static void cylinderDraw()
{
}


//static uint16_t planetIndex[planet_size];

static void planetInit()
{
    const uint16_t len = planet_size; //144;
    matrixIdentity(&transformation);
    setDrawPosition(FIX32(0), FIX32(0), FIX32(40));

    matrixTransform(&transformation, planet, pts_3D, len);
    //vectors3DProjects(pts_3D, FIX32(15), planet_pts_2D, len);
    vectors3DProjectsToPoly(pts_3D, FIX32(15), planet_pts_quad, len);

}

static void planetDraw()
{
    const Vector3D *vector2d = planet_pts_2D;
    const Vector3D *norm = planet_normal;
    const uint16_t clipping_mode = 0x0;
    int gouraud_table_address = GOURAND_PLANET_ADDR;
    uint16_t *color = planet_colors;
    sprite_struct quad;

    uint16_t len = planet_size + cylinder_size;

    quad.bank = RGB16(0x10, 0x10, 0x10); //gray
    quad.gouraud_addr = gouraud_table_address;
    quad.attr = /*(3 << 9) | */ 4; //use gouraud shading

    // Apply matrix only for normals
    matrixIdentity(&transformation);
    setDrawPosition(FIX32(0), FIX32(0), FIX32(40));
    matrixRotate(&transformation, r, r++, r);

    // do planet
    setDrawPosition(FIX32(0), FIX32(0), FIX32(40));
    matrixTransform(&transformation, planet, pts_3D, planet_size);
    vectors3DProjectsToPoly(pts_3D, FIX32(15), planet_pts_quad, planet_size);

    // do rings    
    matrixTransform(&transformation, cylinder, pts_3D, cylinder_size);
    polygon_struct * polygon_dst = &planet_pts_quad[planet_size];
    vectors3DProjectsToPoly(pts_3D, FIX32(15), polygon_dst, cylinder_size);

    
    int i = 0;
    for (i = 0; i < len; i++)
    {
        planet_indices[i] = i;
    }
    // sort by z
    qsort(planet_indices, len, sizeof(uint16_t), compareVectorByZIndices);

    Vector3D eyePosition;
    Vector3D _eyePosition;
    eyePosition.x = FIX32(SCREEN_WIDTH / 2);
    eyePosition.y = FIX32(SCREEN_HEIGHT / 2);
    eyePosition.z = 0;
    matrixTransform(&transformation, &eyePosition, &_eyePosition, 1);

    Vector3D _lightPosition;
    Vector3D lightPosition;
    // real ...
    _lightPosition.x = FIX32(0.9);
    _lightPosition.y = FIX32(0.9);
    _lightPosition.z = FIX32(-0.2);

    // calculate inv matrix + light
    Matrix3D inv;
    matrixInv(&transformation, &inv); // matrixIdentity(&transformation);
    matrixVectorRotate(&inv, &_lightPosition, &lightPosition);

    int idx = 0;
    while (len--)
    {
        sprite_struct *poly = &planet_pts_quad[planet_indices[idx]];
        u16 *p = (u16 *)(VDP1_RAM + gouraud_table_address);

        // fill vertices...
        poly->bank = RGB16(0x10, 0x10, 0x10); //gray
        poly->gouraud_addr = gouraud_table_address;
        poly->attr = /*(3 << 9) | */ 4; //use gouraud shading

        // Check back face culling
        //if (isPolygonCulled(poly))
        //if (isNormalVisible(&_eyePosition,norm))
        {
            // Apply lighting or color...
            applyLightingForPolygon(p, norm, &lightPosition, color);

            // draw real polygon
            vdp_draw_polygon(poly);

            gouraud_table_address += 8;
        }

        color += 4;
        norm += 4;
        poly++;
        idx++;
    }
}

static void sphereDraw()
{
    const Vector3D *vector2d = pts_2D;
    const Vector3D *norm = uvsphere_normal;
    const u16 clipping_mode = 0x0;
    const int gouraud_table_address = GOURAND_SPHERE_ADDR;
    u16 *color = uvsphere_colors;
    sprite_struct quad;

    u16 len = uvsphere_size; //144;

    quad.bank = RGB16(0x10, 0x10, 0x10); //gray
    quad.gouraud_addr = gouraud_table_address;
    quad.attr = (3 << 9) | 4; //use gouraud shading

    matrixIdentity(&transformation);
    matrixRotate(&transformation, r, r++, r);
    setDrawPosition(fix32Mul(sinFix32(r), FIX32(300)), fix32Mul(cosFix32(r), FIX32(300)), fix32Mul(sinFix32(r), FIX32(300)));
    //matrixRotate(&transformation, FIX32(PI / 2), FIX32(PI / 2), FIX32(PI / 2));
    //setDrawPosition(FIX32(0), FIX32(0), FIX32(40));

    matrixTransform(&transformation, uvsphere, pts_3D, len);
    //matrixTransform(&transformation, uvsphere_normal, normal_3D, len);
    vectors3DProjects(pts_3D, FIX32(15), pts_2D, len);

    Vector3D eyePosition;
    Vector3D _eyePosition;
    eyePosition.x = FIX32(640 / 2);
    eyePosition.y = FIX32(512 / 2);
    eyePosition.z = 0;
    matrixTransform(&transformation, &eyePosition, &_eyePosition, 1);

    Vector3D _lightPosition;
    Vector3D lightPosition;
    // real ...
    _lightPosition.x = FIX32(0.9);
    _lightPosition.y = FIX32(0.9);
    _lightPosition.z = FIX32(-0.2);
    // normalized
    // _lightPosition.x = FIX32(0.5773502);
    // _lightPosition.y = FIX32(0.5773502);
    // _lightPosition.z = FIX32(-0.5773502);

    // calculate inv matrix + light
    Matrix3D inv;
    matrixInv(&transformation, &inv); // matrixIdentity(&transformation);
    matrixVectorRotate(&inv, &_lightPosition, &lightPosition);

    norm = uvsphere_normal;

    while (len--)
    {
        u16 *p = (u16 *)(VDP1_RAM + quad.gouraud_addr);

        // Copy vertices
        quad.x = vector2d[0].x;
        quad.y = vector2d[0].y;
        quad.x2 = vector2d[1].x;
        quad.y2 = vector2d[1].y;
        quad.x3 = vector2d[2].x;
        quad.y3 = vector2d[2].y;
        quad.x4 = vector2d[3].x;
        quad.y4 = vector2d[3].y;
        vector2d += 4;

        // Check back face culling
        if (isPolygonCulled(&quad))
        //if (isNormalVisible(&_eyePosition,norm))
        {
            // Apply lighting or color...
            applyColorForPolygon(p, norm, &lightPosition, color);

#if 0
            // Attempt to fix aliasing
            // Draw behind first
            // Aliasing ...
            quad.x = quad.x + 1;
            quad.x2 = quad.x2 + 1;
            quad.x3 = quad.x3 + 1;
            quad.x4 = quad.x4 + 1;
            vdp_draw_polygon(&quad);

            quad.y = quad.y + 1;
            quad.y2 = quad.y2 + 1;
            quad.y3 = quad.y3 + 1;
            quad.y4 = quad.y4 + 1;
            vdp_draw_polygon(&quad);
#endif

            // draw real polygon
            vdp_draw_polygon(&quad);

            quad.gouraud_addr += 8;
        }
        color += 4;
        norm += 4;
    }
}

void sceneDraw()
{
    //cubeDraw();
    cylinderDraw();
    //sphereDraw();
    planetDraw();
    //sphereDraw();
}

void sceneInit()
{
    matrixIdentity(&transformation);
    setDrawPosition(FIX32(0), FIX32(0), FIX32(40));
    planetInit();
}
