/*  Copyright 2014 James Laird-Wah

    This file is part of PseudoSaturn.

    PseudoSaturn is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    PseudoSaturn is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PseudoSaturn; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// Modded from pseudo saturn

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <iapetus.h>

static int emulate_bios_loadcd_init(void)
{
    int ret;
    *(uint32_t *)0x6000278 = 0;
    *(uint32_t *)0x600027c = 0;
    if ((ret = cd_abort_file()) != IAPETUS_ERR_OK)
        return ret;
    if ((ret = cd_end_transfer()) != IAPETUS_ERR_OK)
        return ret;
    if ((ret = cd_reset_selector_all()) != IAPETUS_ERR_OK)
        return ret;
    if ((ret = cd_set_sector_size(SECT_2048)) != IAPETUS_ERR_OK)
        return ret;
    return IAPETUS_ERR_OK;
}

static struct region_s
{
    char id;
    const char *key;
} regions[] = {
    {1, "JAPAN"},
    {2, "TAIWAN and PHILIPINES"},
    {4, "USA and CANADA"},
    {5, "BRAZIL"},
    {6, "KOREA"},
    {10, "ASIA PAL area"},
    {12, "EUROPE"},
    {13, "LATIN AMERICA"},
    {0, NULL}};

static const char *get_region_string(void)
{
    char region = *(volatile char *)0x20100033;
    struct region_s *r;
    for (r = regions; r->id; r++)
        if (r->id == region)
            return r->key;
    return NULL;
}

static int set_image_region(u8 *base)
{
    const char *str = get_region_string();
    if (!str)
    {
        return -1;
    }

    // set region header
    memset(base + 0x40, ' ', 0x10);
    base[0x40] = str[0];

    // set region footer
    char *ptr = (char*)(base + 0xe00);
    memset(ptr, ' ', 0x20);
    *(uint32_t *)ptr = 0xa00e0009;
    strcpy(ptr + 4, "For ");
    strcpy(ptr + 8, str);
    char *end = ptr + 4 + strlen(ptr + 4);
    *end = '.';

    return 0;
}

static int emulate_bios_loadcd_read(void)
{
    int ret, i;

    // doesn't matter where
    u8 *ptr = (u8 *)0x6002000;

    ret = cd_read_sector(ptr, 150, SECT_2048, 2048 * 16);
    if (ret < 0)
        return ret;

    // BIOS doesn't like PSEUDO discs ;)
    memcpy(ptr, "SEGA SEGASATURN ", 16);

    ret = set_image_region(ptr);
    if (ret < 0)
        return ret;

    ret = cd_put_sector_data(0, 16);
    if (ret < 0)
        return ret;
    while (!(CDB_REG_HIRQ & HIRQ_DRDY))
    {
    }

    for (i = 0; i < 2048 * 16; i += 4)
        CDB_REG_DATATRNS = *(uint32_t *)(ptr + i);

    if ((ret = cd_end_transfer()) != 0)
        return ret;

    while (!(CDB_REG_HIRQ & HIRQ_EHST))
    {
    }

    *(uint16_t *)0x60003a0 = 1;

    return 0;
}

/**
// if we came back, that's an error!
// error -8: bad region
// error -4: bad security code
// error -1: bad headers
**/
int jhload(void)
{
    int ret;
    if ((ret = emulate_bios_loadcd_init()) < 0)
    {
        return ret;
    }

    if ((ret = emulate_bios_loadcd_read()) < 0)
    {
        return ret;
    }
    ret = bios_loadcd_boot();

    return ret;
}