#ifndef TESTS
#include <iapetus.h>
#endif
#include "sprite-math.h"

static void get_sprite_center(sprite_struct *sprite, short *cx, short *cy)
{
    *cx = (sprite->x2 - sprite->x)/2;
    *cy = (sprite->y3 - sprite->y)/2;
}

void translate_sprite(sprite_struct *sprite, fix32 x, fix32 y)
{
    sprite->x = fix32ToInt(intToFix32(sprite->x) + x);
    sprite->y = fix32ToInt(intToFix32(sprite->y) + y);
    sprite->x2 = fix32ToInt(intToFix32(sprite->x2) + x);
    sprite->y2 = fix32ToInt(intToFix32(sprite->y2) + y);
    sprite->x3 = fix32ToInt(intToFix32(sprite->x3) + x);
    sprite->y3 = fix32ToInt(intToFix32(sprite->y3) + y);
    sprite->x4 = fix32ToInt(intToFix32(sprite->x4) + x);
    sprite->y4 = fix32ToInt(intToFix32(sprite->y4) + y);
}

#define scale_pts(x, s) \
    x = fix32ToInt(fix32Mul(intToFix32(x), s));

void scale_sprite(sprite_struct *sprite, fix32 s)
{
    // naive ...
    scale_pts(sprite->x, s);
    scale_pts(sprite->x2, s);
    scale_pts(sprite->x3, s);
    scale_pts(sprite->x4, s);

    scale_pts(sprite->y, s);
    scale_pts(sprite->y2, s);
    scale_pts(sprite->y3, s);
    scale_pts(sprite->y4, s);
}


#define rotate_ptsX(x, s) \
    x = fix32ToInt(fix32Mul(intToFix32(tx), cosFix32(s))) - fix32ToInt(fix32Mul(intToFix32(ty), sinFix32(s))) + cx;

#define rotate_ptsY(x, s) \
    x = fix32ToInt(fix32Mul(intToFix32(tx), sinFix32(s))) + fix32ToInt(fix32Mul(intToFix32(ty), cosFix32(s))) + cy;

void rotate_sprite(sprite_struct *sprite, fix32 a)
{
    short cx, cy;
    short tx, ty;

    /*
    // translate point to origin
    float tempX = x - cx;
    float tempY = y - cy;

    // now apply rotation
    float rotatedX = tempX*cos(theta) - tempY*sin(theta);
    float rotatedY = tempX*sin(theta) + tempY*cos(theta);

    // translate back
    x = rotatedX + cx;
    y = rotatedY + cy;
*/
    get_sprite_center(sprite, &cx, &cy);

    tx = (sprite->x - cx);
    ty = (sprite->y - cy);
    rotate_ptsX(sprite->x, a);
    rotate_ptsY(sprite->y, a);

    tx = (sprite->x2 - cx);
    ty = (sprite->y2 - cy);
    rotate_ptsX(sprite->x2, a);
    rotate_ptsY(sprite->y2, a);

    tx = (sprite->x3 - cx);
    ty = (sprite->y3 - cy);
    rotate_ptsX(sprite->x3, a);
    rotate_ptsY(sprite->y3, a);

    tx = (sprite->x4 - cx);
    ty = (sprite->y4 - cy);
    rotate_ptsX(sprite->x4, a);
    rotate_ptsY(sprite->y4, a);
    // rotate p
}
