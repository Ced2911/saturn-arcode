#pragma once
#include "sat-math.h"

void translate_sprite(sprite_struct *sprite, fix32 x, fix32 y);
void scale_sprite(sprite_struct *sprite, fix32 s);
void rotate_sprite(sprite_struct *sprite, fix32 a);