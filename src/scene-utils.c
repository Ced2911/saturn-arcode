#include <stdio.h>
#include <string.h>
#include <iapetus.h>
#include "config.h"
#include "sat-math.h"
#include "sprite-math.h"
#include "scene.h"

void set_background_color(uint8_t r, uint8_t g, uint8_t b)
{
    u16 *bg = (u16 *)0x05E7FFFE;
    bg[0] = RGB16(0x0F, 0x0F, 0x0F);
}

void set_clipping()
{
    sprite_struct quad;
    quad.x = 0;
    quad.y = 0;

    vdp_local_coordinate(&quad);
    //system clipping
    quad.x = SCREEN_WIDTH;
    quad.y = SCREEN_HEIGHT;
    quad.x2 = SCREEN_WIDTH;
    quad.y2 = SCREEN_HEIGHT;

    vdp_system_clipping(&quad);

    //user clipping
    quad.x = 0;
    quad.y = 0;
    quad.x2 = SCREEN_WIDTH;
    quad.y2 = SCREEN_HEIGHT;

    vdp_user_clipping(&quad);
}

static int isCulled(int x0, int y0, int x1, int y1, int x2, int y2)
{
    int delta0 = x2 - x0;
    int delta1 = y1 - y0;
    int delta2 = x1 - x0;
    int delta3 = y2 - y0;

    if (delta0 * delta1 < delta2 * delta3)
    {
        return 1;
    }
    return 0;
}

int isPolygonCulled(sprite_struct *quad)
{
    return (isCulled(quad->x, quad->y, quad->x2, quad->y2, quad->x3, quad->y3)) || (isCulled(quad->x, quad->y, quad->x3, quad->y3, quad->x4, quad->y4));
}

int isNormalVisible(Vector3D *position, Vector3D *norm)
{
    fix32 dp;
    u8 col = 0;
    dp = fix32Mul(position->x, norm->x) +
         fix32Mul(position->y, norm->y) +
         fix32Mul(position->z, norm->z);

    return dp > 0;
}

uint8_t colorForPoint(Vector3D *lightPosition, Vector3D *norm)
{
    fix32 dp;
    u8 col = 0;
    dp = fix32Mul(lightPosition->x, norm->x) +
         fix32Mul(lightPosition->y, norm->y) +
         fix32Mul(lightPosition->z, norm->z);

    if (dp > 0)
    {
        col = dp >> (FIX32_FRAC_BITS - 4);
        if (col > 31)
        {
            col = 31;
        }
    }

    return col;
}

int isPolygonValid(sprite_struct *quad)
{
    int delta0 = quad->x3 - quad->x;
    int delta1 = quad->y2 - quad->y;
    int delta2 = quad->x2 - quad->x;
    int delta3 = quad->y3 - quad->y;

    // check if it's zero coord
    if (quad->x == 0 || quad->x2 == 0 || quad->x3 == 0 || quad->x4 == 0)
    {
        return 0;
    }

    if (delta0 > 500 || delta1 < -500)
    {
        return 0;
    }
    return 1;
}





void applyLightingForPolygon(uint16_t *dst, Vector3D *norm, Vector3D *lightPosition, uint16_t *color) {

// set color based on normal
#if 1
    *dst++ = color[0];
    *dst++ = color[1];
    *dst++ = color[2];
    *dst++ = color[3];
#elif 0
    *dst++ = color[0];
    *dst++ = color[0];
    *dst++ = color[0];
    *dst++ = color[0];
#else // lighting
    uint8_t c0, c1, c2, c3;
    c0 = colorForPoint(lightPosition, &norm[0]);
    c1 = colorForPoint(lightPosition, &norm[1]);
    c2 = colorForPoint(lightPosition, &norm[2]);
    c3 = colorForPoint(lightPosition, &norm[3]);

#if 0
    *dst++ = RGB16(c0, c0, c0);
    *dst++ = RGB16(c1, c1, c1);
    *dst++ = RGB16(c2, c2, c2);
    *dst++ = RGB16(c3, c3, c3);
#else
    *dst++ = RGB16(c0, c0, c0);
    *dst++ = RGB16(c0, c0, c0);
    *dst++ = RGB16(c0, c0, c0);
    *dst++ = RGB16(c0, c0, c0);
#endif
#endif
}

void applyColorForPolygon(uint16_t *dst, Vector3D *norm, Vector3D *lightPosition, uint16_t *color)
{
/*
    
*/

// set color based on normal
#if 1
    *dst++ = color[0];
    *dst++ = color[1];
    *dst++ = color[2];
    *dst++ = color[3];
#elif 0
    *dst++ = color[0];
    *dst++ = color[0];
    *dst++ = color[0];
    *dst++ = color[0];
#else // lighting
    uint8_t c0, c1, c2, c3;
    c0 = colorForPoint(lightPosition, &norm[0]);
    c1 = colorForPoint(lightPosition, &norm[1]);
    c2 = colorForPoint(lightPosition, &norm[2]);
    c3 = colorForPoint(lightPosition, &norm[3]);

#if 0
    *dst++ = RGB16(c0, c0, c0);
    *dst++ = RGB16(c1, c1, c1);
    *dst++ = RGB16(c2, c2, c2);
    *dst++ = RGB16(c3, c3, c3);
#else
    *dst++ = RGB16(c0, c0, c0);
    *dst++ = RGB16(c0, c0, c0);
    *dst++ = RGB16(c0, c0, c0);
    *dst++ = RGB16(c0, c0, c0);
#endif
#endif
}


/**************************************************************************
 *************************************************************************/ 



#include "model.h"

#define MAX_POINTS 4000

static Vector3D pts_3D[MAX_POINTS];
static Vector3D pts_2D[MAX_POINTS];

void modelDraw(model_t * model, Matrix3D * transformation, uint32_t gouraud_table_address)
{
    const Vector3D *vector2d = pts_2D;
    const Vector3D *norm = model->normals;
    const u16 clipping_mode = 0x0;
    u16 *color = model->colors;
    sprite_struct quad;

    u16 len = model->polyCount; //144;

    quad.bank = RGB16(0x10, 0x10, 0x10); //gray
    quad.gouraud_addr = gouraud_table_address;
    quad.attr = clipping_mode | 4; //use gouraud shading

    matrixTransform(&transformation, model->vertices, pts_3D, len);
    vectors3DProjects(pts_3D, FIX32(15), pts_2D, len);

    Vector3D eyePosition;
    Vector3D _eyePosition;
    eyePosition.x = FIX32(640 / 2);
    eyePosition.y = FIX32(512 / 2);
    eyePosition.z = 0;
    matrixTransform(&transformation, &eyePosition, &_eyePosition, 1);

    Vector3D _lightPosition;
    Vector3D lightPosition;
    // real ...
    _lightPosition.x = FIX32(0.9);
    _lightPosition.y = FIX32(0.9);
    _lightPosition.z = FIX32(-0.2);

    // calculate inv matrix + light
    Matrix3D inv;
    matrixInv(&transformation, &inv);
    matrixVectorRotate(&inv, &_lightPosition, &lightPosition);

    while (len--)
    {
        u16 *p = (u16 *)(VDP1_RAM + quad.gouraud_addr);

        // Copy vertices
        quad.x = vector2d[0].x;
        quad.y = vector2d[0].y;
        quad.x2 = vector2d[1].x;
        quad.y2 = vector2d[1].y;
        quad.x3 = vector2d[2].x;
        quad.y3 = vector2d[2].y;
        quad.x4 = vector2d[3].x;
        quad.y4 = vector2d[3].y;
        vector2d += 4;

        // Check back face culling
        if (isPolygonCulled(&quad))
        //if (isNormalVisible(&_eyePosition,norm))
        {
            // Apply lighting or color...
            applyColorForPolygon(p, norm, &lightPosition, color);

            // draw real polygon
            vdp_draw_polygon(&quad);

            quad.gouraud_addr += 8;
        }
        color += 4;
        norm += 4;
    }
}