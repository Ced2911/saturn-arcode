
set(SRC 
	${CMAKE_SOURCE_DIR}/src/boot_cd.c
	${CMAKE_SOURCE_DIR}/src/pseudo-saturn/jhloader.c
	${CMAKE_SOURCE_DIR}/src/main.c
	${CMAKE_SOURCE_DIR}/src/sat-math.c
	${CMAKE_SOURCE_DIR}/src/sat-math-table.c
	${CMAKE_SOURCE_DIR}/src/sprite-math.c
	${CMAKE_SOURCE_DIR}/src/draw.c
	${CMAKE_SOURCE_DIR}/src/scene.c
	${CMAKE_SOURCE_DIR}/src/scene-utils.c
	)


add_definitions(-DAPP_VERSION=\"${APP_VERSION}\")
add_definitions(-DREENTRANT_SYSCALLS_PROVIDED)
add_definitions(-DMISSING_SYSCALL_NAMES)

include_directories(${CMAKE_BINARY_DIR}/src)

