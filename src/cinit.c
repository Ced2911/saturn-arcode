
#define		SystemWork		0x060ffc00		/* System Variable Address */
#define		SystemSize		(0x06100000-0x060ffc00)		/* System Variable Size */
extern unsigned int _bstart, _bend;
extern void ss_main( void );

// GNUSH: void to int
int	main( void )
{
	unsigned char	*dst;
	unsigned int	i;

	/* 1.Zero Set .bss Section */
	for( dst = (unsigned char *)&_bstart; dst < (unsigned char *)&_bend; dst++ ) {
		*dst = 0;
	}
	/* 2.ROM has data at end of text; copy it. */

	/* 3.SGL System Variable Clear */
	for( dst = (unsigned char *)SystemWork, i = 0;i < SystemSize; i++) {
		*dst = 0;
	}

	/* Application Call */
	ss_main();
}
