#!/bin/bash

sh2sdk_path='/f/pseudosaturn-master/sh-elf'

export PATH=$PATH:$sh2sdk_path:"$sh2sdk_path/bin"

cmake -G "Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE=../saturn.toolchain ..